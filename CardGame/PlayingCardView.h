//
//  PlayingCardView.h
//  SuperCard
//
//  Created by Kanver Bains on 3/3/13.
//  Copyright (c) 2013 Kanver Bains. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlayingCardView.h"

@interface PlayingCardView : UIView

@property (nonatomic) NSUInteger rank;
@property (strong, nonatomic) NSString *suit;

@property (nonatomic) BOOL faceUp;

- (void)pinch:(UIPinchGestureRecognizer *)gesture;

@end
