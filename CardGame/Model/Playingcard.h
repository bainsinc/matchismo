//
//  Playingcard.h
//  CardGame
//
//  Created by Kanver Bains on 1/27/13.
//  Copyright (c) 2013 Kanver Bains. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Card.h"

@interface PlayingCard : Card

@property(strong, nonatomic) NSString *suit;
@property(nonatomic) NSUInteger rank;

+ (NSArray *)validSuits;
+ (NSUInteger)maxRank;


@end
