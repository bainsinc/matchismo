//
//  PlayingCardDeck.h
//  CardGame
//
//  Created by Kanver Bains on 1/27/13.
//  Copyright (c) 2013 Kanver Bains. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Deck.h"

@interface PlayingCardDeck : Deck


@end
