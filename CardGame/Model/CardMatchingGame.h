//
//  CardMatchingGame.h
//  CardGame
//
//  Created by Kanver Bains on 1/31/13.
//  Copyright (c) 2013 Kanver Bains. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Card.h"
#import "Deck.h"

@interface CardMatchingGame : NSObject

@property (readonly, nonatomic) int score;

-(id)initWithCardCount:(NSUInteger)count usingDeck:(Deck *)deck;

-(void)flipCardAtIndex:(NSUInteger)index;


-(Card *)cardAtIndex:(NSUInteger)index;




@end
