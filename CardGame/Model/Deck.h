//
//  Deck.h
//  CardGame
//
//  Created by Kanver Bains on 1/27/13.
//  Copyright (c) 2013 Kanver Bains. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Card.h"
@class Card;
@class Deck;

@interface Deck : NSObject

-(void)addCard:(Card *)card atTop:(BOOL)atTop;

-(Card *)drawRandomCard;

@end

