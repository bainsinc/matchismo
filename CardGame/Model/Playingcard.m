//
//  Playingcard.m
//  CardGame
//
//  Created by Kanver Bains on 1/27/13.
//  Copyright (c) 2013 Kanver Bains. All rights reserved.
//

#import "Playingcard.h"

@interface PlayingCard ()

@end


@implementation PlayingCard
@synthesize suit = _suit;

-(int)match:(NSArray *)otherCards;
{
    int score = 0;
    
    if ([otherCards count] == 1) {
        id otherCard = [otherCards lastObject];
        if ([otherCard isKindOfClass:[PlayingCard class]]) {
            PlayingCard *otherPlayingcard = (PlayingCard *)otherCard;
            if ([otherPlayingcard.suit isEqualToString:self.suit]) {
                score = 1;
            } else if (otherPlayingcard.rank == self.rank){
                score = 4;
            }
        }
        
}
    
    return score;
}

+ (NSArray *)validSuits
{
    return @[@"♥",@"♣",@"♦",@"♠"];
}

+ (NSArray *)rankStrings
{
    return @[@"?",@"A",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"J",@"Q",@"K"];
}

+(NSUInteger)maxRank {
    return [self rankStrings].count-1; }

- (void)setRank:(NSUInteger)rank
{
    if (rank <= [PlayingCard maxRank]) {
        _rank = rank;
    }
}



- (NSString *)contents
{
    NSArray *rankStrings = @[@"?",@"A",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"J",@"Q",@"K"];
    return [rankStrings[self.rank] stringByAppendingString:self.suit];
}

-(void)setSuit:(NSString *)suit
{
    if ([@[@"♥",@"♣",@"♦",@"♠"] containsObject:suit]) {
        _suit = suit;
    }
}

-(NSString *)suit
{
    return _suit ? _suit : @"?";
}


@end

