//
//  PlayingCardDeck.m
//  CardGame
//
//  Created by Kanver Bains on 1/27/13.
//  Copyright (c) 2013 Kanver Bains. All rights reserved.
//


#import "PlayingCardDeck.h"
#import "Playingcard.h"

@implementation PlayingCardDeck

-(id)init
{
    self = [super init];
    
    if (self) {
        for (NSString *suit in [PlayingCard validSuits]) {
            for (NSUInteger rank = 1; rank <= [PlayingCard maxRank]; rank++) {
                PlayingCard *card = [[PlayingCard alloc] init];
                card.rank = rank;
                card.suit = suit;
                [self addCard:card atTop:NO];
            }
        }
        
    }
    
    return self;
}

@end

