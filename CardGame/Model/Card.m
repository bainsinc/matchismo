//
//  Card.m
//  CardGame
//
//  Created by Kanver Bains on 1/27/13.
//  Copyright (c) 2013 Kanver Bains. All rights reserved.
//

#import "Card.h"

@interface Card()
@end

@implementation Card

-(int)match:(NSArray *)otherCards
{
    int score = 0;
    
    for (Card *Card in otherCards) {
        if ([Card.contents isEqualToString:self.contents]) {
            score = 1;
        }
    }
    return score;
}

@end

