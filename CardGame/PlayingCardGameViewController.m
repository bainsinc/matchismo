//
//  PlayingCardGameViewController.m
//  CardGame
//
//  Created by Kanver Bains on 3/8/13.
//  Copyright (c) 2013 Kanver Bains. All rights reserved.
//

#import "PlayingCardGameViewController.h"
#import "PlayingCardDeck.h"
#import "PlayingCard.h"
#import "PlayingCardCollectionViewCell.h"
@class Card;

@interface PlayingCardGameViewController ()

@end

@implementation PlayingCardGameViewController

- (Deck *)createDeck
{
    return [[PlayingCardDeck alloc] init];
}
-(NSUInteger)startingCardCount
{
    return 52;
}
/*-(void)updateCell:(UICollectionViewCell *)cell usingCard:(Card *)card animate:(BOOL) animate
{ 
    if ([cell isKindOfClass:[PlayingCardCollectionViewCell class]]) {
       PlayingCardView *playingCardView = ((PlayingCardCollectionViewCell *)cell).playingCardView;
        if ([card isKindOfClass:[PlayingCard class]]) {
            PlayingCard *playingCard = (PlayingCard *)card;
            playingCardView.rank = playingCard.rank;
            playingCardView.suit = playingCard.suit;
            playingCardView.faceUp = playingCard.isFaceUp;
            playingCardView.alpha = playingCard.isUnplayable ? 0.3 : 1.0;
            
        }
    }
}*/
-(void)updateCell:(UICollectionViewCell *)cell usingCard:(Card *)card tapped:(BOOL)tapped
{
    if ([cell isKindOfClass:[PlayingCardCollectionViewCell class]])
    {
        PlayingCardView *playingCardView = ((PlayingCardCollectionViewCell *)cell).playingCardView;
        if ([card isKindOfClass:[PlayingCard class]])
        {
            PlayingCard *playingCard = (PlayingCard*)card;
            
            if (tapped)
            {
                
                NSUInteger option;
                if (card.isFaceUp)
                    option = UIViewAnimationOptionTransitionFlipFromLeft;
                else
                    option = UIViewAnimationOptionTransitionFlipFromRight;
                
                [UIView transitionWithView:playingCardView
                                  duration:0.5
                                   options:option
                                animations:^{
                                    playingCardView.rank = playingCard.rank;
                                    playingCardView.suit = playingCard.suit;
                                    playingCardView.faceUp = playingCard.isFaceUp;
                                    playingCardView.alpha = playingCard.isUnplayable ? 0.5 : 1.0;
                                }
                                completion:^(BOOL finished){}];
            }
            
            if (!tapped && !playingCard.isUnplayable && playingCardView.faceUp)
                [NSTimer scheduledTimerWithTimeInterval:0.7
                                                 target:self
                                               selector:@selector(noMatchFlipDown:)
                                               userInfo:(PlayingCardView *)playingCardView
                                                repeats:NO];
            
            if (playingCard.isUnplayable)
            {
                [NSTimer scheduledTimerWithTimeInterval:0.6
                                                 target:self
                                               selector:@selector(makeTransparent:)
                                               userInfo:(PlayingCardView *)playingCardView
                                                repeats:NO];
            }
        }
    }
}


-(void)loadCell:(UICollectionViewCell *)cell usingCard:(Card *)card
{
    if ([cell isKindOfClass:[PlayingCardCollectionViewCell class]])
    {
        PlayingCardView *playingCardView = ((PlayingCardCollectionViewCell *)cell).playingCardView;
        if ([card isKindOfClass:[PlayingCard class]])
        {
            PlayingCard *playingCard = (PlayingCard*)card;
            playingCardView.rank = playingCard.rank;
            playingCardView.suit = playingCard.suit;
            playingCardView.faceUp = playingCard.isFaceUp;
            playingCardView.alpha = playingCard.isUnplayable ? 0.5 : 1.0;
        }
    }
}


-(void)makeTransparent:(NSTimer *)timer
{
    PlayingCardView * playingCardView = timer.userInfo;
    playingCardView.alpha = 0.5;
}


-(void)noMatchFlipDown:(NSTimer *)timer
{
    PlayingCardView * playingCardView = timer.userInfo;
    if(playingCardView.faceUp)
        [UIView transitionWithView:playingCardView
                          duration:0.5
                           options:UIViewAnimationOptionTransitionFlipFromRight
                        animations:^{
                            playingCardView.faceUp = false;
                        }
                        completion:^(BOOL finished){}];
}
@end
