//
//  GameResult.h
//  CardGame
//
//  Created by Kanver Bains on 2/25/13.
//  Copyright (c) 2013 Kanver Bains. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <iAd/iAd.h>

@interface GameResult : NSObject <ADBannerViewDelegate>

+ (NSArray *)allGameResults;

@property (readonly, nonatomic) NSDate *start;
@property (readonly, nonatomic) NSDate *end;
@property (readonly, nonatomic) NSTimeInterval duration;
@property (nonatomic) int score;



- (NSComparisonResult)compareScoreToGameResult:(GameResult *)otherResult;
- (NSComparisonResult)compareEndDateToGameResult:(GameResult *)otherResult;
- (NSComparisonResult)compareDurationToGameResult:(GameResult *)otherResult;


@end
