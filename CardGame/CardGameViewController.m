//
//  CardGameViewController.m
//  CardGame
//
//  Created by Kanver Bains on 1/27/13.
//  Copyright (c) 2013 Kanver Bains. All rights reserved.
//

#import "CardGameViewController.h"
#import "PlayingCardDeck.h"
#import "CardMatchingGame.h"
#import "GameResult.h"
#import "PlayingCardView.h"
#import "Deck.h"
#import "Playingcard.h"
#import "PlayingCardCollectionViewCell.h"

@class Card;
@class Deck;

@interface CardGameViewController () <UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *flipsLabel;
@property (nonatomic) int flipCount;
@property (strong, nonatomic) CardMatchingGame *game;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (strong, nonatomic) GameResult *gameResult;
@property (weak, nonatomic) IBOutlet UICollectionView *cardCollectionView;
@property (weak, nonatomic) IBOutlet PlayingCardView *playingCardView;
@property (strong, nonatomic) Deck *deck;

@end

@implementation CardGameViewController

- (Deck *)deck
{
    if (!_deck) _deck = [[PlayingCardDeck alloc] init];
    return _deck;
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.startingCardCount;
}

/*-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PlayingCard" forIndexPath:indexPath];
    Card *card = [self.game cardAtIndex:indexPath.item];
    [self updateCell:cell usingCard:card animate:NO];
    return cell;
}*/
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PlayingCard" forIndexPath:indexPath];
    Card * card = [self.game cardAtIndex:indexPath.item];
    [self loadCell:cell usingCard:card];
    return cell;
}

-(void)updateCell:(UICollectionViewCell *)cell usingCard:(Card *)card tapped:(BOOL) tapped
{
    //abstract
}
-(void)loadCell:(UICollectionViewCell *)cell usingCard:(Card *)card
{
    //abstract
}


-(GameResult *)gameResult
{
    if (!_gameResult) _gameResult = [[GameResult alloc]init];
    return _gameResult;
}


- (IBAction)reset:(id)sender {
    self.game = nil;
    self.gameResult = nil;
    self.flipCount = 0;
    [self updateUI];
    
}
-(void)updateUI
{
    for (UICollectionViewCell *cell in [self.cardCollectionView visibleCells])
    {
        NSIndexPath *indexPath = [self.cardCollectionView indexPathForCell:cell];
        
        Card *card = [self.game cardAtIndex:indexPath.item];
        [self updateCell:cell usingCard:card tapped:YES];
    }
    self.scoreLabel.text = @"Score: 0";
    self.flipsLabel.text = @"Flips: 0";
}

-(CardMatchingGame *)game
{ 
    if (!_game) _game = [[CardMatchingGame alloc] initWithCardCount:self.startingCardCount usingDeck:[self createDeck]];
    return _game;
}

- (Deck *)createDeck { return nil; }

/*-(void)updateUI
{
    for (UICollectionViewCell *cell in [self.cardCollectionView visibleCells]) {
        NSIndexPath *indexPath = [self.cardCollectionView indexPathForCell:cell];
        Card *card = [self.game cardAtIndex:indexPath.item];
        [self updateCell:cell usingCard:card animate:NO];
    }
    self.scoreLabel.text = [NSString stringWithFormat:@"Score: %d", self.game.score];
}*/
-(void)updateUI:(int)index
{
    for (UICollectionViewCell *cell in [self.cardCollectionView visibleCells]) {
        NSIndexPath *indexPath = [self.cardCollectionView indexPathForCell:cell];
        
        BOOL tapped = NO;
        if (indexPath.item == index)
            tapped = YES;
        
        Card *card = [self.game cardAtIndex:indexPath.item];
        [self updateCell:cell usingCard:card tapped:tapped];
    }
    self.scoreLabel.text = [NSString stringWithFormat:@"Score: %d", self.game.score];
}

- (void)setPlayingCardView:(PlayingCardView *)playingCardView
{
    _playingCardView = playingCardView;
    [self drawRandomCard];
    [playingCardView addGestureRecognizer:[[UIPinchGestureRecognizer alloc] initWithTarget:playingCardView
                                                                                    action:@selector(pinch:)]];
}


- (void)drawRandomCard
{
    Card *card = [self.deck drawRandomCard];
    if ([card isKindOfClass:[PlayingCardView class]]) {
        PlayingCardView *playingcard = (PlayingCardView *)card;
        self.playingCardView.rank = playingcard.rank;
        self.playingCardView.suit = playingcard.suit;
    }
    
    
}


-(void)setFlipCount:(int)flipCount;
{
    _flipCount = flipCount;
    self.flipsLabel.text = [NSString stringWithFormat:@"Flips: %d", self.flipCount];
    self.gameResult.score = self.game.score;
    
    
}

/*- (IBAction)swipe:(UISwipeGestureRecognizer *)sender
{
    [UICollectionView transitionWithView:self.cardCollectionView
                duration:0.5
                                 options:UIViewAnimationOptionTransitionFlipFromLeft
                              animations:^{
    CGPoint tapLocation = [sender locationInView:self.cardCollectionView];
    NSIndexPath *indexPath = [self.cardCollectionView indexPathForItemAtPoint:tapLocation];
    if (indexPath) {
        [self.game flipCardAtIndex:indexPath.item];
        self.flipCount++;
        [self updateUI:indexPath.item];
        self.gameResult.score = self.game.score;
    }
                              }
                              completion:NULL];
}*/


/*- (IBAction)flipCard:(UITapGestureRecognizer *)gesture
{
    [UICollectionView transitionWithView:self.playingCardView
                                duration:0.5
                                 options:UIViewAnimationOptionTransitionFlipFromLeft
                              animations:^{
                                  CGPoint tapLocation = [gesture locationInView:self.playingCardView];
                                  NSIndexPath *indexPath = [self.cardCollectionView indexPathForItemAtPoint:tapLocation];
                                  if (indexPath) {
                                      [self.game flipCardAtIndex:indexPath.item];
                                      self.flipCount++;
                                      [self updateUI:indexPath.item];
                                      self.gameResult.score = self.game.score;
                                  }
                              }
                              completion:NULL];
}*/
- (IBAction)flipCard:(UITapGestureRecognizer *)gesture
{
    CGPoint tapLocation = [gesture locationInView:self.cardCollectionView];
    NSIndexPath *indexPath = [self.cardCollectionView indexPathForItemAtPoint:tapLocation];
    if (indexPath && ![self.game cardAtIndex:indexPath.item].isUnplayable)
    {
        [self.game flipCardAtIndex:indexPath.item];
        self.flipCount ++;
        [self updateUI:indexPath.item];
        self.gameResult.score = self.game.score;

        // other stuff that you've may put in here
    }
    
}

@end
