//
//  PlayingCardCollectionViewCell.h
//  CardGame
//
//  Created by Kanver Bains on 3/8/13.
//  Copyright (c) 2013 Kanver Bains. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlayingCardView.h"

@interface PlayingCardCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet PlayingCardView *playingCardView;


@end
