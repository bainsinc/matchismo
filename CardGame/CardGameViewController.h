//
//  CardGameViewController.h
//  CardGame
//
//  Created by Kanver Bains on 1/27/13.
//  Copyright (c) 2013 Kanver Bains. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Deck.h"
@class Card;
@class Deck;


@interface CardGameViewController : UIViewController

-(Deck *)createDeck;
@property (nonatomic) NSUInteger startingCardCount;
//-(void)updateCell:(UICollectionViewCell *)cell usingCard:(Card *)card animate:(BOOL)animate;
-(void)updateCell:(UICollectionViewCell *)cell usingCard:(Card *)card tapped:(BOOL)tapped; //abstract
-(void)loadCell:(UICollectionViewCell *)cell usingCard:(Card *)card; //abstract

@end

